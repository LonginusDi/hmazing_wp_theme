<?php
/**
 *	Aurum WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */

global $s;

$shop_cart_counter_ajax = get_data('shop_cart_counter_ajax');
$shop_cart_show_on_hover = get_data('shop_cart_show_on_hover');

if( ! get_data('header_links'))
	return;
?>
<div class="header-links">

	<ul class="header-widgets">
		<?php if(get_data('header_links_search_form')): ?>
		<li>

			<form action="<?php echo home_url(); ?>" method="get" class="search-form<?php echo $s ? ' input-visible' : ''; ?>" enctype="application/x-www-form-urlencoded">

				<div class="search-input-env<?php echo trim(lab_get('s')) ? ' visible' : ''; ?>">
					<input type="text" class="form-control search-input" name="s" placeholder="<?php _e('Search...', 'aurum'); ?>" value="<?php echo esc_attr($s); ?>">
				</div>

				<a href="#" class="search-btn">
					<?php echo lab_get_svg('images/search.svg'); ?>
					<span class="sr-only"><?php _e('Search', 'aurum'); ?></span>
				</a>

			</form>

		</li>
		<?php endif; ?>
		<li>
			<a href="/wishlist">
				<svg class="new-icon" height="28px" viewBox="0 0 24 24" width="28px" xmlns="http://www.w3.org/2000/svg">
				    <path d="M0 0h24v24H0z" fill="none"/>
				    <path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/>
				</svg>
			</a>
		</li>
		<?php if(get_data('header_cart_info') && function_exists('WC')): ?>
		<?php
			$cart_items_count = WC()->cart->get_cart_contents_count();
			$cart_icon = get_data('header_cart_info_icon');

			if( ! $cart_icon)
				$cart_icon = 1;

			if($shop_cart_counter_ajax)
			{
				$cart_items_count = 0;
			}

		?>
		<li>
			<a class="cart-counter<?php echo $cart_items_count ? ' has-notifications' : ''; echo $shop_cart_counter_ajax ? ' cart-counter-ajax' : ''; echo $shop_cart_show_on_hover ? ' hover-activated' : ''; echo apply_filters( 'aurum_mini_cart_direct_link', false ) ? ' direct-link' : ''; ?>" href="<?php echo WC()->cart->get_cart_url(); ?>">
				<span class="badge items-count"><?php echo $cart_items_count; ?></span>
				<?php echo lab_get_svg("images/cart_{$cart_icon}.svg"); ?>
			</a>

			<div class="lab-mini-cart">
				<?php if($shop_cart_counter_ajax): ?>
					<div class="cart-is-loading"><?php _e('Loading cart contents...', 'aurum'); ?></div>
				<?php else: ?>
					<?php get_template_part('tpls/woocommerce-mini-cart'); ?>
				<?php endif; ?>
			</div>
		</li>
		<?php endif; ?>
	</ul>

</div>